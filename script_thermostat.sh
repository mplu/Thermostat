#!/bin/sh

#EXEC_FOLDER="/path/to/folder/where/exec/runs"
#INTERFACE_FOLDER="/path/to/folder/where/export/import/data"

#define local folder
EXEC_FOLDER="/home/matthieu/dev_soft/Thermostat"
INTERFACE_FOLDER="/media/thermo"
METEO_FILE="rouen.txt"

#define api url with city ID and API user ID
API_URL="https://api.openweathermap.org/data/2.5/weather?id=[CITYID]&appid=[APIUSERID]"

cd $EXEC_FOLDER

#check for preexisting data and erase it
if [ -f $METEO_FILE ];then
	echo "file exist ! suppressing it";
	rm $METEO_FILE*
else
	echo "file does not exist !"
fi

#getting openweather map data with API
wget --no-check-certificate $API_URL -O $METEO_FILE

if [ -f $METEO_FILE ];then
	echo "file received !";
	
else
	echo "impossible to find data !"
fi

#copying settling point data file to open with thermostat application
cp $INTERFACE_FOLDER/consigne.csv .

#getting a random file from "fond" folder, to have a background image
FOND_FILE=`ls fond | shuf -n 1`
cp fond/"$FOND_FILE" fond.bmp

#starting thermostat application and injected output in a sqlite database
sudo ./thermostat |
while read -r DATE JOUR HEURE NOMBRE1 NOMBRE2 NOMBRE3 NOMBRE4 NOMBRE5 TEXTE
do
	#echo "$DATE" "$JOUR" "$HEURE" "$NOMBRE1" "$NOMBRE2" "$NOMBRE3" "$NOMBRE4" "$NOMBRE5" "$TEXTE"
	sqlite3  $EXEC_FOLDER/meteosat.db "insert into meteo values('$DATE','$JOUR','$HEURE','$NOMBRE1','$NOMBRE2','$NOMBRE3','$NOMBRE4','$NOMBRE5','$TEXTE');"
done

#updating picture (built by thermostat application) on local SPI screen
sudo kill -9 `pgrep fbi`
sudo fbi -T 2 -d /dev/fb1 -noverbose -a out.jpg

#backuping datas to ewxternal folder
cp out.jpg $INTERFACE_FOLDER/
cp meteosat.db $INTERFACE_FOLDER/


