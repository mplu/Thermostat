#include "includes.h"

int GetTemperatureFromProbe(char* filename,float* temperature)
{
	int i;
	char chaine[LIGNE_MAX][TAILLE_MAX] = {{0}};
	int ret = RET_ERR;
	int check_frame = 0;
	int found = 0;
	int temp_int = 0;
	FILE* file_sonde = NULL;
	
	file_sonde = fopen(filename, "r");	
	if (file_sonde != NULL)
	{	
		i = 0;
		while ((fgets(chaine[i], TAILLE_MAX, file_sonde) != NULL) && (i < LIGNE_MAX))
		{
			//printf("%s\n",chaine);
			i++;
		}

		// file exemple
		//fc 00 4b 46 7f ff 04 10 a6 : crc=a6 YES
		//fc 00 4b 46 7f ff 04 10 a6 t=15750
		i=2;
		check_frame = 0;
		
		
		// Search the YES indicate that transfer was ok
		while((chaine[LIGNE_CRC][i]!='\0') && (found == 0))
		{	
			i++;
			if(chaine[LIGNE_CRC][i-2]=='Y' && chaine[LIGNE_CRC][i-1]=='E' && chaine[LIGNE_CRC][i]=='S')
			{
				check_frame = 1;			
			}
		}
		if (check_frame == 1)
		{
			i=1;
			found = 0;
			while((chaine[LIGNE_TEMPER][i]!='\0') && (found == 0))
			{	
				i++;
				if(chaine[LIGNE_TEMPER][i-1]=='t' && chaine[LIGNE_TEMPER][i]=='=')
				{
					found = 1;
					temp_int = 		(chaine[LIGNE_TEMPER][i+1] - 0x30) * 10000
								+	(chaine[LIGNE_TEMPER][i+2] - 0x30) * 1000	
								+	(chaine[LIGNE_TEMPER][i+3] - 0x30) * 100	
								+	(chaine[LIGNE_TEMPER][i+4] - 0x30) * 10	
								+	(chaine[LIGNE_TEMPER][i+5] - 0x30) * 1	;				
				}
			}
			if(found == 1)
			{
				fclose(file_sonde); 
				*temperature = (float)temp_int / 1000.0 ;
				//printf("Temperature for %s : %f\r\n",filename,*temperature);
				ret = RET_OK;
			}else
			{
				printf("Temperature not found\n");
				ret = RET_ERR;
			}
		}else
		{
			printf("CRC not ok\n");
			ret = RET_ERR;
		}
	}else
	{
		printf("No temp file found ok\n");
		ret = RET_ERR;
	}
	
	return ret;
}

int GetConsigneFromFile(char* filename,char** table_bin,float * consigne_jour,float * consigne_absolu)
{
	int ret = RET_ERR;
	char Table[j_jour_size][TAILLE_MAX] ; // Cha�ne vide de taille TAILLE_MAX
	char Ligne1[TAILLE_MAX] ; // Cha�ne vide de taille TAILLE_MAX
	char Ligne2[TAILLE_MAX] ; // Cha�ne vide de taille TAILLE_MAX
	e_jour index;
	char * pch;
	FILE* fichier = NULL;
	char temp[TAILLE_MAX];
	int temp1,temp2,i;
	
	fichier = fopen(filename, "r");
    if (fichier != NULL)
    {
		
		fgets(Ligne1, TAILLE_MAX, fichier);
		fgets(Ligne2, TAILLE_MAX, fichier);
		for(index = j_Lundi; index<j_jour_size;index++)
		{
			fgets(Table[index], TAILLE_MAX, fichier);
		}
        fclose(fichier);
		
		pch=strchr(Ligne1,';');
		temp1 = pch - Ligne1;
		pch=strchr(&Ligne1[temp1+1],';');
		temp2 = pch - Ligne1;
		
		pch=strchr(&Ligne1[temp2+1],';');
		temp2 = pch - Ligne1;
		for(i=0;i<(temp2-temp1);i++)
		{
			temp[i] = Ligne1[i+temp1+1];
		}
		temp[i] = 0;
		sscanf (temp,"%f;%f",consigne_jour,consigne_absolu);
		
		for(index = j_Lundi; index<j_jour_size;index++)
		{
			pch=strchr(Table[index],';');
			temp1 = pch - Table[index] +1;
			for(i=0;i<48;i++)
			{
				
				if(Table[index][temp1 + i*2] == 'O')
				{
					table_bin[index][i] = 1;
				}else
				{
					table_bin[index][i] = 0;
				}
			}
		}
		ret = RET_OK;
    }
	
	return ret;
	
}

int GetDataFromNOAA(char* filename,s_noaa* datastruct)
{
	
	int ret = RET_ERR;
	te_info index;
	int tab_ligne[te_info_size] ;
	char *tab_TAGS[te_info_size] = {TAG_VENT,TAG_VISIBILITE,TAG_SKYCOND,TAG_WEATHER,TAG_TEMPER,TAG_DEWPOINT,TAG_HUMIDIE,TAG_PRESSION};
	int i,j,offset,nb_lignes_lues;
	char found = TRUE;
	char fichier_txt[LIGNE_MAX][TAILLE_MAX] = {{0}};
	
	
	
	FILE* file_info = NULL;
    
	
	for(index = info_wind;index<te_info_size;index++)
	{
		tab_ligne[index] = -1;
	}
	
	file_info = fopen(FILE_INFO, "r");

	if (file_info != NULL)
	{
		i = 0;
		while ((fgets(fichier_txt[i], TAILLE_MAX, file_info) != NULL) && (i < LIGNE_MAX))
		{
			i++;
		}
		nb_lignes_lues = i;


        //r�cuperation de la date et heure
        offset = -1;
        for(i=0;i<TAILLE_MAX;i++)
        {
            if (fichier_txt[LIGNE_DATE][i] == '/')
            {
                offset = i+ 2;
                i = TAILLE_MAX+1;
            }
        }
        if(offset!=-1)
        {
            sscanf (&(fichier_txt[LIGNE_DATE][offset]),"%s %s",datastruct->date,datastruct->heure);
        }
		
		for(j=2;j<nb_lignes_lues;j++)
		{
			//identification de chaque entete de ligne
			for(index = info_wind;index<te_info_size;index++)
			{
				found = TRUE;
				//test de chaque TAG
				//sprintf(string_tmp,"%s",tab_TAGS[index]);
				for(i=0;i<sizeof(tab_TAGS[index])-1;i++)
				{
					if(fichier_txt[j][i] == tab_TAGS[index][i])
					{

					}else
					{
						found = FALSE;
						break;
					}
				}
				
				if(found == TRUE)
				{
					tab_ligne[index] = j;
					break;
				}
			}
			
		}

        //recuperation vent
		if(tab_ligne[info_wind] != -1)
		{
			offset = -1;
			for(i=0;i<TAILLE_MAX;i++)
			{
				if ((fichier_txt[tab_ligne[info_wind]][i] == 'a')&&(fichier_txt[tab_ligne[info_wind]][i+1] == 't'))
				{
					offset = i+ 2;
					i = TAILLE_MAX+1;
				}
			}
			if(offset!=-1)
			{
				sscanf (&(fichier_txt[tab_ligne[info_wind]][offset]),"%d",&(datastruct->vent));
			}
			datastruct->vent = (int)((float)datastruct->vent*_1_MPH);
		}

        //recuperation temperature
		if(tab_ligne[info_temperature] != -1)
		{
			offset = -1;
			for(i=0;i<TAILLE_MAX;i++)
			{
				if ((fichier_txt[tab_ligne[info_temperature]][i] == 'F')&&(fichier_txt[tab_ligne[info_temperature]][i+1] == ' ')&&(fichier_txt[tab_ligne[info_temperature]][i+2] == '('))
				{
					offset = i+ 3;
					i = TAILLE_MAX+1;
				}
			}
			if(offset!=-1)
			{
				sscanf (&(fichier_txt[tab_ligne[info_temperature]][offset]),"%d",&(datastruct->temperature));
			}
		}

        //recuperation humidity
		if(tab_ligne[info_humidity] != -1)
		{
			offset = -1;
			for(i=0;i<TAILLE_MAX;i++)
			{
				if ((fichier_txt[tab_ligne[info_humidity]][i] == ':'))
				{
					offset = i+ 1;
					i = TAILLE_MAX+1;
				}
			}
			if(offset!=-1)
			{
				sscanf (&(fichier_txt[tab_ligne[info_humidity]][offset]),"%d",&(datastruct->humidite));
			}
		}

		ret = RET_OK;
		fclose(file_info);
	}
	
	
	return ret;
	
}

int GetDataFromMeteocielfr(char* filename,s_noaa* datastruct)
{
	int ret = RET_ERR;
	int i,offset,nb_lignes_lues,ligne_meteo;
	char * ptr_meteo;
	char fichier_txt[LIGNE_MAX][TAILLE_MAX] = {{0}};
	char str_ligne_meteo[3*TAILLE_MAX] = {0};
	
	//printf("debut\n");
	
	FILE* file_info = NULL;
    
	
	
	file_info = fopen(FILE_INFO, "r");

	if (file_info != NULL)
	{
		//printf("fichier ouvert\n");
		i = 0;
		while ((fgets(fichier_txt[i], TAILLE_MAX, file_info) != NULL) && (i < LIGNE_MAX))
		{
			i++;
		}
		nb_lignes_lues = i;
		//printf("nb ligne : %d\n",nb_lignes_lues);

		for(i=0;i<nb_lignes_lues;i++)
		{
			//printf("cherche dans ligne %d -- %s\n",i,fichier_txt[i]);
			ptr_meteo = strstr (fichier_txt[i],"tr> <tr> <td align=center>");
			if(ptr_meteo!=NULL)
			{
				
				ligne_meteo = i;
				offset = ptr_meteo-fichier_txt[ligne_meteo]  ;
				//printf("meteo trouv� ligne : %d, offset %d\n",ligne_meteo,ptr_meteo-fichier_txt[ligne_meteo]);
				//printf("1--%s\n",fichier_txt[ligne_meteo]);
				//printf("2--%s\n",fichier_txt[ligne_meteo+1]);
				//printf("3--%s\n",fichier_txt[ligne_meteo+2]);
				i = LIGNE_MAX+1;
			}
		}
		//copie de la ligne meteo + les 2 suivantes

		strcpy (str_ligne_meteo,fichier_txt[ligne_meteo]);
		strcat  (str_ligne_meteo,fichier_txt[ligne_meteo+1]);
		strcat  (str_ligne_meteo,fichier_txt[ligne_meteo+2]);
		//printf("total--%s\n",str_ligne_meteo);
		
		//cherche heure acquisition
		offset = offset+sizeof("tr> <tr> <td align=center>")-1;
		sscanf (&(str_ligne_meteo[offset]),"%d",&(datastruct->heure_int));
		//printf("heure : %d\n",datastruct->heure_int);
		
		// by pass neb
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset neb %d\n",offset);
		}
		
		// by pass temps
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset neb %d\n",offset);
		}
		
		// by pass visi
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset neb %d\n",offset);
		}
		
		//cherche temperature
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset temperature %d\n",offset);
while ((str_ligne_meteo[offset] < 0x30) || (str_ligne_meteo[offset] > 0x39))
            {
                offset++;
            }
			sscanf (&(str_ligne_meteo[offset]),"%f",&(datastruct->temperature_float));
			//printf("temperature : %.1f\n",datastruct->temperature_float);
		}
		// cherche humidit�
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset humidite %d\n",offset);
			sscanf (&(str_ligne_meteo[offset]),"%d",&(datastruct->humidite));
			//printf("humidite : %d\n",datastruct->humidite);
		}
		
		// by pass humidex
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset humidex %d\n",offset);
		}
		
		// by pass windchill
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset windchill %d\n",offset);
		}
		
		// cherche vent 1/3
		// by pass premiere case
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset vent 1/3 %d\n",offset);
		}
		
		// cherche vent 2/3
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"<div align=center>");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("<div align=center>")-1;
			//printf("offset vent 2/3 %d\n",offset);
			sscanf (&(str_ligne_meteo[offset]),"%d",&(datastruct->vent));
			//printf("vent : %d\n",datastruct->vent);
		}
		// cherche vent 3/3
		ptr_meteo = strstr (&(str_ligne_meteo[offset]),"(");
		if(ptr_meteo!=NULL)
		{
			offset = ptr_meteo-str_ligne_meteo+sizeof("(")-1;
			//printf("offset vent 3/3 %d\n",offset);
			sscanf (&(str_ligne_meteo[offset]),"%d",&(datastruct->vent_rafale));
			//printf("vent rafale: %d\n",datastruct->vent_rafale);
		}
		
		//for(i=0;i<10;i++)
		//{
		//	printf("%c",str_ligne_meteo[offset+i]);
		//}printf("\n");
		
		// by pass pression
		
		// by pass precipitation
		

		ret = RET_OK;
		fclose(file_info);
	}else
	{
		//printf("erreur ouverture fichier\n");
	}
	
	//printf("fin\n");
	
	
	return ret;
	
}

int GetDataFromOpenWeatherMap(char* filename,s_noaa* datastruct)
{
	int ret = RET_ERR;
	int i,nb_lignes_lues;
	char * ptr_meteo;
	char fichier_txt[LIGNE_MAX][TAILLE_MAX] = {{0}};
	
	//printf("debut\n");
	
	FILE* file_info = NULL;
    
	
	
	file_info = fopen(FILE_INFO, "r");

	if (file_info != NULL)
	{
		//printf("fichier ouvert\n");
		i = 0;
		while ((fgets(fichier_txt[i], TAILLE_MAX, file_info) != NULL) && (i < LIGNE_MAX))
		{
			i++;
		}
		nb_lignes_lues = i;

		for(i=0;i<nb_lignes_lues;i++)
		{
			float levent = 0;
			struct tm * ptm;
			//temperature
			ptr_meteo = strstr (fichier_txt[i],"temp");
			sscanf (ptr_meteo+strlen("temp\":"),"%f",&(datastruct->temperature_float));
			datastruct->temperature_float-=273.15;
			
			//humidite
			ptr_meteo = strstr (fichier_txt[i],"humidity");
			sscanf (ptr_meteo+strlen("humidity\":"),"%d",&(datastruct->humidite));
			
			//vent
			ptr_meteo = strstr (fichier_txt[i],"wind");
			sscanf (ptr_meteo+strlen("wind\":{\"speed\":"),"%f",&levent);
			datastruct->vent = levent * 3.6;
			
			//date
			ptr_meteo = strstr (fichier_txt[i],"dt");
			sscanf (ptr_meteo+strlen("dt\":"),"%d",&(datastruct->heure_int));
			
			ptm = gmtime ( &(datastruct->heure_int) );

 			datastruct->heure_int=ptm->tm_hour+1;		
			
		}

		ret = RET_OK;
		fclose(file_info);
	}else
	{
		//printf("erreur ouverture fichier\n");
	}
	
	//printf("fin\n");
	
	
	return ret;
	
}
