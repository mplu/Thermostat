#define THERMO_PIN 		4
//#define THERMO_PIN 		6
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <wiringPi.h>

#define BUFLEN 512  //Max length of buffer
#define PORT 8001   //The port on which to listen for incoming data


int main(int argc, char** argv)
{
    printf("Hello world!\n");
    struct sockaddr_in serv_addr;
    int sockfd, slen=sizeof(serv_addr);
   
    char message[BUFLEN];
	
    
    if(argc != 2)
    {
      printf("Usage : %s <Server-IP>\n",argv[0]);
	  return 0;
    }
	wiringPiSetup () ;
 
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
        fprintf(stderr, "socket");
 
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if (inet_aton(argv[1], &serv_addr.sin_addr)==0)
    {
        fprintf(stderr, "inet_aton() failed\n");
        return 0;
    }

    //start communication
    //while(1)
    {
		if(digitalRead (THERMO_PIN) == 0)
		{	
			message[0] = '1' ;
		}else
		{
			message[0] = '0' ;
		}
        printf("GPIO Value : %c\n",message[0]);
        
		
		
        //send the message
//        if (sendto(sockfd, message, strlen(message) , 0 , (struct sockaddr *) &serv_addr, slen) == -1)
        if (sendto(sockfd, message, 1 , 0 , (struct sockaddr *) &serv_addr, slen) == -1)
        {
            fprintf(stderr, "sendto()");
        }
    }
    close(sockfd);
	return 0;
}
